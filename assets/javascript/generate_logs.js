const looper = async (loops = 1, callback) => {
  for (let step = 0; step < loops; step++) {
    await callback()
  }
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export default async function generateLogs() {
  // Generate some normal logs
  await looper(5, async () => {
    await sleep(1000)
    fetch('')
  })

  // Generate error logs
  await looper(30, () => {
    const randomString = Math.random()
      .toString(36)
      .substring(2, 15)
    fetch(`/api/user/${randomString}`)
  })

  // Generate some more normal logs
  await looper(10, async () => {
    await sleep(1000)
    fetch('')
  })
}
