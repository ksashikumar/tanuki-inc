const consola = require('consola')

export default function(req, res, next) {
  res.on('finish', () => {
    const logger = res.statusCode < 400 ? consola.log : consola.error
    logger({
      status: res.statusCode,
      url: req.originalUrl
    })
  })

  next()
}
